package models

type Product struct {
	Id         string
	Name       string
	Price      float64
	CategoryId string
	Barcode    string
	Created_at string `json:"created_at"`
	Updated_at string `json:"updated_at"`
}

type CreateProduct struct {
	Name       string
	Price      float64
	CategoryId string
	Barcode    string
}

type Barcode struct {
	SaleId   string
	Barcode  string
	Quantity int
}
