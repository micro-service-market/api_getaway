package models

type Category struct {
	Id         string
	Name       string
	ParentId   string
	Created_at string `json:"created_at"`
	Updated_at string `json:"updated_at"`
}

type CreateCategory struct {
	Name     string
	ParentId string
}
