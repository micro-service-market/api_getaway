package models

type BranchTransaction struct {
	Id         string
	StaffId    string
	BranchId   string
	ProductId  string
	Type       string
	Price      float64
	Quantity   int
	Created_at string `json:"created_at"`
	Updated_at string `json:"updated_at"`
}

type CreateBranchTransaction struct {
	StaffId   string
	BranchId  string
	ProductId string
	Type      string
	Price     float64
	Quantity  int
}
