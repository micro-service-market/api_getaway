package models

type BranchProduct struct {
	Id         string
	ProductId  string
	BranchId   string
	Count      int
	Created_at string `json:"created_at"`
	Updated_at string `json:"updated_at"`
}

type CreateBranchProduct struct {
	ProductId string
	BranchId  string
	Count     int
}
