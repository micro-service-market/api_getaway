package models

type SaleProduct struct {
	Id         string
	SaleId     string
	ProductId  string
	Price      float64
	Quantity   int
	Created_at string `json:"created_at"`
	Updated_at string `json:"updated_at"`
}

type CreateSaleProduct struct {
	SaleId    string
	ProductId string
	Price     float64
	Quantity  int
}
