package handler

import (
	"fmt"
	"main/genproto/sale_service"
	"main/genproto/staff_service"
	"main/models"
	"main/packages/logger"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Router       /end [PUT]
// @Summary      Update Sale
// @Description  Update Sale
// @Tags         END
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of sale"
// @Param        data  body      models.EndSale  true  "Sale data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) EndSale(c *gin.Context) {

	var end models.EndSale
	err := c.ShouldBindJSON(&end)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	id := c.Param("id")

	if end.Status == "Cancel" {
		fmt.Println("Sale Canceled")
		return
	}

	respSaleProduct, err := h.grpcClient.SaleProductService().GetAllSaleId(c.Request.Context(), &sale_service.SaleId{SaleId: id})

	if err != nil {
		fmt.Println("error SaleProduct Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	var price float64

	for _, v := range respSaleProduct.SaleProducts {
		price += float64(v.Price) * float64(v.Quantity)
	}

	resp, err := h.grpcClient.SaleService().Update(c.Request.Context(), &sale_service.Sale{
		Id:              id,
		BranchId:        end.BranchId,
		ShopAssistentId: end.ShopAssistentId,
		CashierId:       end.CashierId,
		ClientName:      end.ClientName,
		PaymentType:     end.PaymentType,
		Status:          end.Status,
		Price:           float32(price),
	})

	if err != nil {
		fmt.Println("error Sale Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	for _, v := range respSaleProduct.SaleProducts {
		_, err := h.grpcClient.BranchTransactionService().Create(c.Request.Context(), &sale_service.CreateBranchTransaction{
			BranchId:  end.BranchId,
			StaffId:   end.CashierId,
			ProductId: v.ProductId,
			Type:      "plus",
			Price:     v.Price,
			Quantity:  v.Quantity,
		})

		if err != nil {
			fmt.Println("error BranchTransaction Create:", err.Error())
			c.JSON(http.StatusInternalServerError, "internal server error")
			return
		}
	}

	cashier, err := h.grpcClient.StaffService().Get(c.Request.Context(), &staff_service.IdRequest{Id: end.CashierId})

	if err != nil {

		fmt.Println("error from import cashier:", err.Error())

		return
	}

	// tarifCashier, err := h.strg.StaffTarif().GetStaffTarif(c.Request.Context(), models.IdRequest{ID: cashier.TarifId})
	tarifCashier, err := h.grpcClient.TarifService().Get(c.Request.Context(), &staff_service.IdRequest{Id: cashier.TarifId})

	if err != nil {

		fmt.Println("error from import tariff:", err.Error())

		return
	}

	amount := 0.0

	if tarifCashier.Type == "Fixed" {

		if end.PaymentType == "Cash" {
			amount = float64(tarifCashier.AmountForCash)
		} else {
			amount = float64(tarifCashier.AmountForCard)
		}

		//h.strg.Staff().ChangeBalance(cashierId, amount)

	} else if tarifCashier.Type == "Percent" {

		if end.PaymentType == "Cash" {
			amount = (price * float64(tarifCashier.AmountForCash)) / 100
		} else {
			amount = (price * float64(tarifCashier.AmountForCard)) / 100
		}

		//h.strg.Staff().ChangeBalance(cashierId, amount)

	}

	reqUpB := models.UpdateBalanceRequest{
		SaleId: id,
		Cashier: models.StaffType{
			StaffId: end.CashierId,
			Amount:  amount,
		},
		TransactionType: "Topup",
		SourceType:      "Sales",
		Text:            "plus",
	}

	if end.ShopAssistentId != "" {

		// shopAssistant, err := h.strg.Staff().GetStaff(c.Request.Context(), models.IdRequest{ID: sale.ShopAssistentId})
		shopAssistant, err := h.grpcClient.StaffService().Get(c.Request.Context(), &staff_service.IdRequest{Id: end.ShopAssistentId})

		if err != nil {

			fmt.Println("error from import shopAssistent:", err.Error())

			return
		}

		// tarifAssistent, err := h.strg.StaffTarif().GetStaffTarif(c.Request.Context(), models.IdRequest{ID: shopAssistant.TarifId})
		tarifAssistent, err := h.grpcClient.TarifService().Get(c.Request.Context(), &staff_service.IdRequest{Id: shopAssistant.TarifId})

		if err != nil {

			fmt.Println("error from import tariff:", err.Error())

			return
		}

		amount := 0.0

		if tarifAssistent.Type == "Fixed" {

			if end.PaymentType == "Cash" {
				amount = float64(tarifAssistent.AmountForCash)
			} else {
				amount = float64(tarifAssistent.AmountForCard)
			}

			//h.strg.Staff().ChangeBalance(shopAssistentId, amount)

		} else if tarifAssistent.Type == "Percent" {

			if end.PaymentType == "Cash" {
				amount = (price * float64(tarifAssistent.AmountForCash)) / 100
			} else {
				amount = (price * float64(tarifAssistent.AmountForCard)) / 100
			}

			//h.strg.Staff().ChangeBalance(shopAssistentId, amount)
		}

		reqUpB.ShopAssistent.StaffId = end.ShopAssistentId
		reqUpB.ShopAssistent.Amount = amount
	}

	// h.strg.Staff().UpdateBalance(c.Request.Context(), reqUpB)
	h.grpcClient.StaffService().UpdateBalance(c.Request.Context(), &staff_service.UpdateBalanceRequest{
		TransactionType: reqUpB.TransactionType,
		SourceType:      reqUpB.SourceType,
		SaleId:          reqUpB.SaleId,
		Text:            reqUpB.Text,
		ShopAssistent: &staff_service.StaffType{
			StaffId: reqUpB.ShopAssistent.StaffId,
			Amount:  float32(reqUpB.ShopAssistent.Amount),
		},
		Cashier: &staff_service.StaffType{
			StaffId: reqUpB.Cashier.StaffId,
			Amount:  float32(reqUpB.Cashier.Amount),
		},
	})

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete BranchProduct in cache")
	}
}
