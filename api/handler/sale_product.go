package handler

import (
	"errors"
	"fmt"

	"main/api/response"
	"main/genproto/branch_service"
	"main/genproto/sale_service"
	"main/models"
	"main/packages/helper"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /sale_products [POST]
// @Summary      Create SaleProduct
// @Description  Create SaleProduct
// @Tags         SALE_PRODUCT
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateSaleProduct  true  "SaleProduct data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateSaleProduct(c *gin.Context) {

	var product models.CreateSaleProduct
	err := c.ShouldBindJSON(&product)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	respSale, err := h.grpcClient.SaleService().Get(c.Request.Context(), &sale_service.IdRequest{Id: product.SaleId})

	if err != nil {
		fmt.Println("error Sale Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	respReminder, err := h.grpcClient.BranchProductService().GetProduct(c.Request.Context(), &branch_service.GetProductRequest{
		BranchId:  respSale.BranchId,
		ProductId: product.ProductId,
	})

	if err != nil {
		fmt.Println("error Reminder Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	respCheckProduct, err := h.grpcClient.SaleProductService().GetBySaleId(c.Request.Context(), &sale_service.SaleIdRequest{
		SaleId:    product.SaleId,
		ProductId: product.ProductId,
	})
	if err != nil {
		if respReminder.Count >= int64(product.Quantity) {
			resp, err := h.grpcClient.SaleProductService().Create(c.Request.Context(), &sale_service.CreateSaleProduct{
				SaleId:    product.SaleId,
				ProductId: product.ProductId,
				Price:     float32(product.Price),
				Quantity:  int64(product.Quantity),
			})

			if err != nil {
				fmt.Println("error SaleProduct Create:", err.Error())
				c.JSON(http.StatusInternalServerError, "internal server error")
				return
			}
			c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
			return
		} else {
			fmt.Println("Not Enough Product", err.Error())
			c.JSON(http.StatusBadRequest, "not enough product")
			return
		}
	}

	if respReminder.Count >= int64(product.Quantity)+respCheckProduct.Quantity {

		resp, err := h.grpcClient.SaleProductService().Update(c.Request.Context(), &sale_service.SaleProduct{
			Id:        respCheckProduct.Id,
			SaleId:    respCheckProduct.SaleId,
			ProductId: respCheckProduct.ProductId,
			Quantity:  respCheckProduct.Quantity + int64(product.Quantity),
			Price:     respCheckProduct.Price,
		})

		if err != nil {
			fmt.Println("error SaleProduct Update:", err.Error())
			c.JSON(http.StatusInternalServerError, "internal server error")
			return
		}

		c.JSON(http.StatusOK, resp)

	} else {
		fmt.Println("Not Enough Product", err.Error())
		c.JSON(http.StatusBadRequest, "not enough product")
		return
	}
	// respProduct, err := h.grpcClient.ProductService().Get(c.Request.Context(), &catalog_service.IdReqRes{Id: product.ProductId})

	// if err != nil {
	// 	fmt.Println("error Product Get:", err.Error())
	// 	c.JSON(http.StatusInternalServerError, "internal server error")
	// 	return
	// }

}

// @Router       /sale_products/{id} [put]
// @Summary      Update SaleProduct
// @Description  api for update SaleProduct
// @Tags         SALE_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of SaleProduct"
// @Param        product    body     models.CreateSaleProduct  true  "data of product"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateSaleProduct(c *gin.Context) {

	var product models.CreateSaleProduct
	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.SaleProductService().Update(c.Request.Context(), &sale_service.SaleProduct{
		Id:        id,
		SaleId:    product.SaleId,
		ProductId: product.ProductId,
		Quantity:  int64(product.Quantity),
		Price:     float32(product.Price),
	})

	if err != nil {
		fmt.Println("error SaleProduct Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete SaleProduct in cache")
	}

}

// @Router       /sale_products/{id} [GET]
// @Summary      Get By Id
// @Description  get SaleProduct by ID
// @Tags         SALE_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "SaleProduct ID" format(uuid)
// @Success      200  {object}  models.BranchProduct
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetSaleProduct(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.SaleProduct{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting SaleProduct in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.SaleProductService().Get(c.Request.Context(), &sale_service.IdRequest{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error SaleProduct Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create SaleProduct in cache")
	}

}

// @Router       /sale_products [get]
// @Summary      List SaleProduct
// @Description  get SaleProduct
// @Tags         SALE_PRODUCT
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Success      200  {array}   models.SaleProduct
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllSaleProduct(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.SaleProductService().GetAll(c.Request.Context(), &sale_service.GetAllSaleProductRequest{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.log.Error("error SaleProduct GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllSaleProduct")
	c.JSON(http.StatusOK, resp)
}

// @Router       /sale_products/{id} [DELETE]
// @Summary      DELETE BY ID
// @Description  delete SaleProduct by ID
// @Tags         Sale_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "SaleProduct ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteSaleProduct(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error sale-product GetAll:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}

	resp, err := h.grpcClient.SaleProductService().Delete(c.Request.Context(), &sale_service.IdRequest{Id: id})

	if err != nil {
		h.log.Error("error sale-product GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete sale-product in cache")
	}
}
