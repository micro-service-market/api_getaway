package handler

import (
	"errors"
	"fmt"

	"main/api/response"
	"main/genproto/catalog_service"
	"main/models"
	"main/packages/helper"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /products [POST]
// @Summary      Create Product
// @Description  Create Product
// @Tags         PRODUCT
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateProduct  true  "Product data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateProduct(c *gin.Context) {

	var product models.CreateProduct
	err := c.ShouldBindJSON(&product)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.ProductService().Create(c.Request.Context(), &catalog_service.CreateProduct{
		Name:       product.Name,
		Price:      float32(product.Price),
		Barcode:    product.Barcode,
		CategoryId: product.CategoryId,
	})

	if err != nil {
		fmt.Println("error Product Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /products/{id} [put]
// @Summary      Update Product
// @Description  api for update product
// @Tags         PRODUCT
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of product"
// @Param        product    body     models.CreateProduct  true  "data of product"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateProduct(c *gin.Context) {

	var product models.CreateProduct
	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.ProductService().Update(c.Request.Context(), &catalog_service.Product{
		Id:         id,
		Name:       product.Name,
		Price:      float32(product.Price),
		Barcode:    product.Barcode,
		CategoryId: product.CategoryId,
	})

	if err != nil {
		fmt.Println("error Product Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete product in cache")
	}

}

// @Router       /products/{id} [GET]
// @Summary      Get By Id
// @Description  get product by ID
// @Tags         PRODUCT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Product ID" format(uuid)
// @Success      200  {object}  models.Product
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetProduct(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.Product{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting category in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.ProductService().Get(c.Request.Context(), &catalog_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Category Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create product in cache")
	}

}

// @Router       /products [get]
// @Summary      List Product
// @Description  get Product
// @Tags         PRODUCT
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        name    query     string  false  "filter by name"
// @Success      200  {array}   models.Product
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllProduct(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.ProductService().GetAll(c.Request.Context(), &catalog_service.GetAllProductRequest{
		Page:  int64(page),
		Limit: int64(limit),
		Name:  c.Query("name"),
	})

	if err != nil {
		h.log.Error("error Product GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllProduct")
	c.JSON(http.StatusOK, resp)
}

// @Router       /products/{id} [DELETE]
// @Summary      DELETE BY ID
// @Description  delete product by ID
// @Tags         PRODUCT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Product ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteProduct(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error product GetAll:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}

	resp, err := h.grpcClient.ProductService().Delete(c.Request.Context(), &catalog_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error product GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete product in cache")
	}
}
