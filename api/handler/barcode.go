package handler

import (
	"fmt"
	"main/api/response"
	"main/genproto/branch_service"
	"main/genproto/catalog_service"
	"main/genproto/sale_service"
	"main/models"
	"main/packages/logger"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Router       /barcode [POST]
// @Summary      Create SaleProduct
// @Description  Create SaleProduct
// @Tags         BARCODE
// @Accept       json
// @Produce      json
// @Param        data  body      models.Barcode  true  "SaleProduct data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) Barcode(c *gin.Context) {

	var product models.Barcode
	err := c.ShouldBindJSON(&product)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	respProduct, err := h.grpcClient.ProductService().GetByBarcode(c.Request.Context(), &catalog_service.GetByBarcodereq{Barcode: product.Barcode})

	if err != nil {
		fmt.Println("error Product Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	respSale, err := h.grpcClient.SaleService().Get(c.Request.Context(), &sale_service.IdRequest{Id: product.SaleId})

	if err != nil {
		fmt.Println("error Sale Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	respReminder, err := h.grpcClient.BranchProductService().GetProduct(c.Request.Context(), &branch_service.GetProductRequest{
		BranchId:  respSale.BranchId,
		ProductId: respProduct.Id,
	})

	if err != nil {
		fmt.Println("error Reminder Get:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	respCheckProduct, err := h.grpcClient.SaleProductService().GetBySaleId(c.Request.Context(), &sale_service.SaleIdRequest{
		SaleId:    product.SaleId,
		ProductId: respProduct.Id,
	})

	if err != nil {
		if respReminder.Count >= int64(product.Quantity) {
			resp, err := h.grpcClient.SaleProductService().Create(c.Request.Context(), &sale_service.CreateSaleProduct{
				SaleId:    product.SaleId,
				ProductId: respProduct.Id,
				Price:     float32(respProduct.Price),
				Quantity:  int64(product.Quantity),
			})

			if err != nil {
				fmt.Println("error SaleProduct Create:", err.Error())
				c.JSON(http.StatusInternalServerError, "internal server error")
				return
			}
			c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
			return
		} else {
			fmt.Println("Not Enough Product", err.Error())
			c.JSON(http.StatusBadRequest, "not enough product")
			return
		}
	}

	if respReminder.Count >= int64(product.Quantity)+respCheckProduct.Quantity {

		resp, err := h.grpcClient.SaleProductService().Update(c.Request.Context(), &sale_service.SaleProduct{
			Id:        respCheckProduct.Id,
			SaleId:    respCheckProduct.SaleId,
			ProductId: respCheckProduct.ProductId,
			Quantity:  respCheckProduct.Quantity + int64(product.Quantity),
			Price:     respCheckProduct.Price,
		})

		if err != nil {
			fmt.Println("error SaleProduct Update:", err.Error())
			c.JSON(http.StatusInternalServerError, "internal server error")
			return
		}

		c.JSON(http.StatusOK, resp)

	} else {
		fmt.Println("Not Enough Product", err.Error())
		c.JSON(http.StatusBadRequest, "not enough product")
		return
	}

}
