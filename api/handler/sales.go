package handler

import (
	"errors"
	"fmt"

	"main/api/response"
	"main/genproto/sale_service"
	"main/genproto/staff_service"
	"main/models"
	"main/packages/helper"
	"main/packages/logger"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateSale godoc
// @Router       /sales [POST]
// @Summary      Create Sale
// @Description  Create Sale
// @Tags         SALE
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateSale  true  "Sale data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateSale(c *gin.Context) {

	var sale models.CreateSale
	err := c.ShouldBindJSON(&sale)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.SaleService().Create(c.Request.Context(), &sale_service.CreateSale{
		BranchId:        sale.BranchId,
		ShopAssistentId: sale.ShopAssistentId,
		CashierId:       sale.CashierId,
		PaymentType:     sale.PaymentType,
		Price:           float32(sale.Price),
		Status:          sale.Status,
		ClientName:      sale.ClientName,
	})
	if err != nil {
		fmt.Println("error Sale Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	cashier, err := h.grpcClient.StaffService().Get(c.Request.Context(), &staff_service.IdRequest{Id: sale.CashierId})

	if err != nil {

		fmt.Println("error from import cashier:", err.Error())

		return
	}

	// tarifCashier, err := h.strg.StaffTarif().GetStaffTarif(c.Request.Context(), models.IdRequest{ID: cashier.TarifId})
	tarifCashier, err := h.grpcClient.TarifService().Get(c.Request.Context(), &staff_service.IdRequest{Id: cashier.TarifId})

	if err != nil {

		fmt.Println("error from import tariff:", err.Error())

		return
	}

	amount := 0.0

	if tarifCashier.Type == "Fixed" {

		if sale.PaymentType == "Cash" {
			amount = float64(tarifCashier.AmountForCash)
		} else {
			amount = float64(tarifCashier.AmountForCard)
		}

		//h.strg.Staff().ChangeBalance(cashierId, amount)

	} else if tarifCashier.Type == "Percent" {

		if sale.PaymentType == "Cash" {
			amount = (sale.Price * float64(tarifCashier.AmountForCash)) / 100
		} else {
			amount = (sale.Price * float64(tarifCashier.AmountForCard)) / 100
		}

		//h.strg.Staff().ChangeBalance(cashierId, amount)

	}

	reqUpB := models.UpdateBalanceRequest{
		SaleId: resp.GetId(),
		Cashier: models.StaffType{
			StaffId: sale.CashierId,
			Amount:  amount,
		},
		TransactionType: "Topup",
		SourceType:      "Sales",
		Text:            "plus",
	}

	if sale.ShopAssistentId != "" {

		// shopAssistant, err := h.strg.Staff().GetStaff(c.Request.Context(), models.IdRequest{ID: sale.ShopAssistentId})
		shopAssistant, err := h.grpcClient.StaffService().Get(c.Request.Context(), &staff_service.IdRequest{Id: sale.ShopAssistentId})

		if err != nil {

			fmt.Println("error from import shopAssistent:", err.Error())

			return
		}

		// tarifAssistent, err := h.strg.StaffTarif().GetStaffTarif(c.Request.Context(), models.IdRequest{ID: shopAssistant.TarifId})
		tarifAssistent, err := h.grpcClient.TarifService().Get(c.Request.Context(), &staff_service.IdRequest{Id: shopAssistant.TarifId})

		if err != nil {

			fmt.Println("error from import tariff:", err.Error())

			return
		}

		amount := 0.0

		if tarifAssistent.Type == "Fixed" {

			if sale.PaymentType == "Cash" {
				amount = float64(tarifAssistent.AmountForCash)
			} else {
				amount = float64(tarifAssistent.AmountForCard)
			}

			//h.strg.Staff().ChangeBalance(shopAssistentId, amount)

		} else if tarifAssistent.Type == "Percent" {

			if sale.PaymentType == "Cash" {
				amount = (sale.Price * float64(tarifAssistent.AmountForCash)) / 100
			} else {
				amount = (sale.Price * float64(tarifAssistent.AmountForCard)) / 100
			}

			//h.strg.Staff().ChangeBalance(shopAssistentId, amount)
		}

		reqUpB.ShopAssistent.StaffId = sale.ShopAssistentId
		reqUpB.ShopAssistent.Amount = amount
	}

	// h.strg.Staff().UpdateBalance(c.Request.Context(), reqUpB)
	h.grpcClient.StaffService().UpdateBalance(c.Request.Context(), &staff_service.UpdateBalanceRequest{
		TransactionType: reqUpB.TransactionType,
		SourceType:      reqUpB.SourceType,
		SaleId:          reqUpB.SaleId,
		Text:            reqUpB.Text,
		ShopAssistent: &staff_service.StaffType{
			StaffId: reqUpB.ShopAssistent.StaffId,
			Amount:  float32(reqUpB.ShopAssistent.Amount),
		},
		Cashier: &staff_service.StaffType{
			StaffId: reqUpB.Cashier.StaffId,
			Amount:  float32(reqUpB.Cashier.Amount),
		},
	})

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})

}

// @Router       /sales/{id} [put]
// @Summary      update sale
// @Description  api for update sales
// @Tags         SALE
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of sale"
// @Param        sale    body     models.CreateSale  true  "data of sale"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateSale(c *gin.Context) {
	var sale models.CreateSale
	err := c.ShouldBindJSON(&sale)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.SaleService().Update(c.Request.Context(), &sale_service.Sale{
		Id:              id,
		BranchId:        sale.BranchId,
		ShopAssistentId: sale.ShopAssistentId,
		CashierId:       sale.CashierId,
		PaymentType:     sale.PaymentType,
		Price:           float32(sale.Price),
		Status:          sale.Status,
		ClientName:      sale.ClientName,
	})

	// resp, err := h.strg.Sales().UpdateSale(c.Request.Context(), id, sale)

	if err != nil {
		fmt.Println("error Sale Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete sale in cache")
	}
}

// @Router       /sales/{id} [GET]
// @Summary      Get By Id
// @Description  get sale by ID
// @Tags         SALE
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Sale ID" format(uuid)
// @Success      200  {object}  models.Sale
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetSale(c *gin.Context) {

	id := c.Param("id")

	var sale = &models.Sale{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, sale)

	if err != nil {
		fmt.Println("Error while geting sale in cache")
	}

	if response {
		c.JSON(http.StatusOK, sale)
		return
	}

	// sale, err = h.strg.Sales().GetSale(c.Request.Context(), models.IdRequest{ID: id})
	resp, err := h.grpcClient.SaleService().Get(c.Request.Context(), &sale_service.IdRequest{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error Sale Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Create(c.Request.Context(), id, resp, 0)

	if err != nil {
		fmt.Println("Error while Create sale in cache")
	}

}

// @Router       /sales/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete sale by Id
// @Tags         SALE
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Sale ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteSale(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error Sale Delete:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}
	// resp, err := h.strg.Sales().DeleteSale(c.Request.Context(), models.IdRequest{ID: id})
	resp, err := h.grpcClient.SaleService().Delete(c.Request.Context(), &sale_service.IdRequest{Id: id})

	if err != nil {
		h.log.Error("error Sale Delete:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete sale in cache")
	}

}

// @Router       /sales [get]
// @Summary      List Sales
// @Description  get Sale
// @Tags         SALE
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Param        client_name    query     string  false  "filter by client_name"
// @Success      200  {array}   models.Sale
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllSale(c *gin.Context) {

	h.log.Info("request GetAllSale")

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	// resp, err := h.strg.Sales().GetAllSale(c.Request.Context(), models.GetAllSaleRequest{
	// 	Page:       page,
	// 	Limit:      limit,
	// 	ClientName: c.Query("client_name"),
	// })

	resp, err := h.grpcClient.SaleService().GetAll(c.Request.Context(), &sale_service.GetAllSaleRequest{
		Page:       int64(page),
		Limit:      int64(limit),
		ClientName: c.Query("client_name"),
	})

	if err != nil {
		h.log.Error("error Sale GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllSale")
	c.JSON(http.StatusOK, resp)

}

/* func (h *Handler) BranchTotal() {
	resp, err := h.strg.Sales().BranchTotal()

	if err != nil {
		fmt.Println("Error geting BranchTotal")
		return
	}

	branches, _ := h.strg.Branch().GetAllBranch(models.GetAllBranchRequest{
		Page:   1,
		Limit:  100,
		Search: "",
	})

	branchesMap := make(map[string]models.Branch)

	for _, v := range branches.Branches {
		branchesMap[v.ID] = v
	}

	for i, v := range resp {
		fmt.Println(branchesMap[i].Name, v.Count, v.Sum)
	}
}

func (h *Handler) GetSalesInDay() {
	resp, err := h.strg.Sales().GetSalesInDay()

	if err != nil {
		fmt.Println("Error geting GetSalesInDay")
		return
	}

	branches, _ := h.strg.Branch().GetAllBranch(models.GetAllBranchRequest{
		Page:   1,
		Limit:  100,
		Search: "",
	})

	branchesMap := make(map[string]models.Branch)

	for _, v := range branches.Branches {
		branchesMap[v.ID] = v
	}

	for branchId, bValue := range resp {
		max := 0.0
		fmt.Println(branchesMap[branchId].Name)
		for _, v := range bValue {
			if max < v {
				max = v
			}
		}
		fmt.Print(max)
	}
}*/
