package handler

import (
	"errors"
	"fmt"

	"main/api/response"
	"main/genproto/branch_service"
	"main/models"
	"main/packages/helper"
	"main/packages/logger"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /branch_products [POST]
// @Summary      Create BranchProduct
// @Description  Create BranchProduct
// @Tags         BRANCH_PRODUCT
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateBranchProduct  true  "Product data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateBranchProduct(c *gin.Context) {

	var product models.CreateBranchProduct
	err := c.ShouldBindJSON(&product)

	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.BranchProductService().Create(c.Request.Context(), &branch_service.CreateBranchProduct{
		ProductId: product.ProductId,
		BranchId:  product.BranchId,
		Count:     int64(product.Count),
	})

	if err != nil {
		fmt.Println("error BranchProduct Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})
}

// @Router       /branch_products/{id} [put]
// @Summary      Update BranchProduct
// @Description  api for update BranchProduct
// @Tags         BRANCH_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of BranchProduct"
// @Param        product    body     models.CreateBranchProduct  true  "data of product"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateBranchProduct(c *gin.Context) {

	var product models.CreateBranchProduct
	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.BranchProductService().Update(c.Request.Context(), &branch_service.BranchProduct{
		Id:        id,
		ProductId: product.ProductId,
		BranchId:  product.BranchId,
		Count:     int64(product.Count),
	})

	if err != nil {
		fmt.Println("error BranchProduct Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete BranchProduct in cache")
	}

}

// @Router       /branch_products/{id} [GET]
// @Summary      Get By Id
// @Description  get BranchProduct by ID
// @Tags         BRANCH_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "BranchProduct ID" format(uuid)
// @Success      200  {object}  models.BranchProduct
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetBranchProduct(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.BranchProduct{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting BranchProduct in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respService, err := h.grpcClient.BranchProductService().Get(c.Request.Context(), &branch_service.IdReqRes{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error BranchProduct Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respService)

	err = h.red.Cache().Create(c.Request.Context(), id, respService, 0)

	if err != nil {
		fmt.Println("Error while Create BranchProduct in cache")
	}

}

// @Router       /branch_products [get]
// @Summary      List BranchProduct
// @Description  get BranchProduct
// @Tags         BRANCH_PRODUCT
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Success      200  {array}   models.BranchProduct
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllBranchProduct(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.BranchProductService().GetAll(c.Request.Context(), &branch_service.GetAllBranchProductRequest{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.log.Error("error BranchProduct GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Warn("response to GetAllBranchProduct")
	c.JSON(http.StatusOK, resp)
}

// @Router       /branch_products/{id} [DELETE]
// @Summary      DELETE BY ID
// @Description  delete BranchProduct by ID
// @Tags         BRANCH_PRODUCT
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "BranchProduct ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteBranchProduct(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error product GetAll:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}

	resp, err := h.grpcClient.BranchProductService().Delete(c.Request.Context(), &branch_service.IdReqRes{Id: id})

	if err != nil {
		h.log.Error("error product GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete branch-product in cache")
	}
}
