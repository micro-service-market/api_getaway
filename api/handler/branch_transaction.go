package handler

import (
	"errors"
	"fmt"

	"main/api/response"
	"main/genproto/sale_service"
	"main/models"
	"main/packages/helper"
	"main/packages/logger"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Router       /branch_transactions [POST]
// @Summary      Create Transaction
// @Description  Create Transaction
// @Tags         BRABCH_TRANSACTION
// @Accept       json
// @Produce      json
// @Param        data  body      models.CreateBranchTransaction  true  "transaction data"
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) CreateBranchTransaction(c *gin.Context) {

	var transaction models.CreateBranchTransaction
	err := c.ShouldBindJSON(&transaction)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	resp, err := h.grpcClient.BranchTransactionService().Create(c.Request.Context(), &sale_service.CreateBranchTransaction{
		BranchId:  transaction.BranchId,
		StaffId:   transaction.StaffId,
		ProductId: transaction.ProductId,
		Type:      transaction.Type,
		Quantity:  int64(transaction.Quantity),
		Price:     float32(transaction.Price),
	})

	if err != nil {
		fmt.Println("error BranchTransaction Create:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	c.JSON(http.StatusCreated, response.CreateResponse{Id: resp.GetId()})

}

// @Router       /branch_transactions/{id} [put]
// @Summary      Update Transaction
// @Description  api for update branch transaction
// @Tags         BRANCH_TRANSACTION
// @Accept       json
// @Produce      json
// @Param        id    path     string  true  "id of transaction"
// @Param        staff    body     models.CreateBranchTransaction  true  "data of transaction"
// @Success      200  {string}   string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) UpdateBranchTransaction(c *gin.Context) {
	var transaction models.CreateBranchTransaction
	err := c.ShouldBindJSON(&transaction)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id := c.Param("id")

	resp, err := h.grpcClient.BranchTransactionService().Update(c.Request.Context(), &sale_service.BranchTransaction{
		Id:        id,
		BranchId:  transaction.BranchId,
		StaffId:   transaction.StaffId,
		ProductId: transaction.ProductId,
		Type:      transaction.Type,
		Quantity:  int64(transaction.Quantity),
		Price:     float32(transaction.Price),
	})

	if err != nil {
		fmt.Println("error Branch Transaction Update:", err.Error())
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete branch-transaction in cache")
	}
}

// @Router       /branch_transactions/{id} [GET]
// @Summary      Get By Id
// @Description  get transaction by ID
// @Tags         BRANCH_TRANSACTION
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Transaction ID" format(uuid)
// @Success      200  {object}  models.BranchTransaction
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetBranchTransaction(c *gin.Context) {

	id := c.Param("id")

	var resp = &models.BranchTransaction{}

	response, err := h.red.Cache().Get(c.Request.Context(), id, resp)

	if err != nil {
		fmt.Println("Error while geting branch-transaction in cache")
	}

	if response {
		c.JSON(http.StatusOK, resp)
		return
	}

	respS, err := h.grpcClient.BranchTransactionService().Get(c.Request.Context(), &sale_service.IdRequest{Id: id})

	if err != nil {
		c.JSON(http.StatusInternalServerError, "internal server error")
		fmt.Println("error BranchTransaction Get:", err.Error())
		return
	}

	c.JSON(http.StatusOK, respS)

	err = h.red.Cache().Create(c.Request.Context(), id, respS, 0)

	if err != nil {
		fmt.Println("Error while Create branch-transaction in cache")
	}

}

// @Router       /branch_transactions/{id} [DELETE]
// @Summary      Delete By Id
// @Description  delete transaction by Id
// @Tags         BRANCH_TRANSACTION
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Transaction ID" format(uuid)
// @Success      200  {string}  string
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) DeleteBranchTransaction(c *gin.Context) {

	id := c.Param("id")

	if !helper.IsValidUUID(id) {

		h.log.Error("error BranchTransaction Delete:", logger.Error(errors.New("invalid id")))
		c.JSON(http.StatusBadRequest, "invalid id")
		return

	}
	resp, err := h.grpcClient.BranchTransactionService().Delete(c.Request.Context(), &sale_service.IdRequest{Id: id})

	if err != nil {
		h.log.Error("error BranchTransaction Delete:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	c.JSON(http.StatusOK, resp)

	err = h.red.Cache().Delete(c.Request.Context(), id)

	if err != nil {
		fmt.Println("Error while delete branch-transaction in cache")
	}

}

// @Router       /branch_transactions [get]
// @Summary      List Transaction
// @Description  get Transaction
// @Tags         TRANSACTION
// @Accept       json
// @Produce      json
// @Param        limit    query     integer  true  "limit for response"  Default(10)
// @Param        page    query     integer  true  "page of req"  Default(1)
// @Success      200  {array}   models.StaffTransaction
// @Failure      400  {object}  response.ErrorResp
// @Failure      404  {object}  response.ErrorResp
// @Failure      500  {object}  response.ErrorResp
func (h *Handler) GetAllBranchTransaction(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		c.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.grpcClient.BranchTransactionService().GetAll(c.Request.Context(), &sale_service.GetAllBranchTransactionRequest{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.log.Error("error BranchTransaction GetAll:", logger.Error(err))
		c.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	c.JSON(http.StatusOK, resp)

}
